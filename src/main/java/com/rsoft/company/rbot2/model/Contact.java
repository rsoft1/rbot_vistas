package com.rsoft.company.rbot2.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String state;
    private String indicative;
    private String number;
    private String message;
    private boolean default_message;

    public Contact() {
    }

    public Contact(String state, String indicative, String number, String message, boolean default_message) {
        this.state = state;
        this.indicative = indicative;
        this.number = number;
        this.message = message;
        this.default_message = default_message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIndicative() {
        return indicative;
    }

    public void setIndicative(String indicative) {
        this.indicative = indicative;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isDefault_message() {
        return default_message;
    }

    public void setDefault_message(boolean default_message) {
        this.default_message = default_message;
    }

    @Override
    public String toString() {
        return "Contact{" + "id=" + id + ", state=" + state + ", indicative=" + indicative + ", number=" + number + ", message=" + message + ", default_message=" + default_message + '}';
    }

}
