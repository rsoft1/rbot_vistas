package com.rsoft.company.rbot2.model.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

import com.rsoft.company.rbot2.model.Config;

public class ConfigDao {

    private final EntityManagerFactory emf;

    public ConfigDao() {
        this.emf = Persistence.createEntityManagerFactory("$objectdb/db/rbot.odb");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public void create(Config config) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(config);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Config get() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Config.class));
            Query q = em.createQuery(cq);
            return (Config) q.getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
 
    public void update(Config config) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.merge(config);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void delete(Long id) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Config contact = null;
            try {
                contact = em.getReference(Config.class, id);
                contact.getId();
            } catch (Exception e) {
                System.out.println("No existe ningun elemento con esa id");
            }
            em.remove(contact);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
