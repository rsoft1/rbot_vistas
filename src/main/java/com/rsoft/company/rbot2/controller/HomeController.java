package com.rsoft.company.rbot2.controller;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.rsoft.company.rbot2.view.Home;
import com.rsoft.company.rbot2.model.dao.ContactDao;
import com.rsoft.company.rbot2.model.Contact;
import com.rsoft.company.rbot2.util.CellRender;
import com.rsoft.company.rbot2.util.Excel;
import com.rsoft.company.rbot2.util.TableModel;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;

/**
 *
 * @author Antabla
 */
public class HomeController {

    //VISTA
    private final Home homeView;

    //DATOS
    private final ContactDao contactDao;
    private List<Contact> contacts;
    private TableModel tableModelContact;

    //AUXILIARES
    private Icon defaultIcon;
    private int selectRowTable;

    public HomeController() {
        homeView = new Home();
        contactDao = new ContactDao();

        selectRowTable = -1;

        homeView.setLocationRelativeTo(null);
        homeView.setVisible(true);
    }

    public void init() {
        initView();
        events();
        setDataTableContacts(null);//si el valor es null el carga los datos de la db
    }

    private void initView() {

        /*Configuramos la tabla de contactos*/
        homeView.tableContacts.getTableHeader().setFont(new Font("Century Gothic", Font.PLAIN, 16));
        homeView.tableContacts.getTableHeader().setOpaque(false);
        homeView.tableContacts.getTableHeader().setBackground(new Color(41, 155, 41));
        homeView.tableContacts.getTableHeader().setForeground(Color.white);

        /*Desactivamos los botones de envio de mensajes hasta que se autentique el usuario*/
        homeView.btnDiffusion.setEnabled(false);
        homeView.btnSend.setEnabled(false);

        /*Guardamos el icono por defecto del codigo QR*/
        defaultIcon = homeView.QR.getIcon();
    }

    private void events() {
        btnCloseClick();
        btnFileChooserClick();
        btnManualEntryClick();
        btnAddContactClick();
        btnDeleteContactClick();
        btnEditContactClick();
        btnChangeStateContactClick();

        tableContactsClick();
        panelContentClick();
    }

    private void setDataTableContacts(List<Contact> newContacts) {
        String[] titles = {"Id", "Estado", "Numero", "Mensaje"};

        contacts = newContacts == null ? contactDao.read(true, 0, 0) : newContacts;

        Object[][] data = new Object[contacts.size()][4];

        for (int i = 0; i < contacts.size(); i++) {
            data[i][0] = contacts.get(i).getId() + "";
            data[i][1] = contacts.get(i).getState();
            data[i][2] = contacts.get(i).getIndicative() + contacts.get(i).getNumber();
            data[i][3] = contacts.get(i).getMessage();
        }

        tableModelContact = new TableModel(titles, data);

        homeView.tableContacts.setModel(tableModelContact);

        for (int i = 0; i < titles.length; i++) {
            homeView.tableContacts.getColumnModel().getColumn(i).setCellRenderer(new CellRender());
        }

        selectRowTable = -1;
    }

    private void btnCloseClick() {
        homeView.btnClose.addActionListener(e -> {
            this.homeView.dispose();
            System.exit(0);
        });
    }

    private void btnFileChooserClick() {

        homeView.btnFileChoose.addActionListener(e -> {

            JFileChooser fc = new JFileChooser();
            int returnVal = fc.showOpenDialog(this.homeView);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();

                if (file.getName().contains(".xlsx")) {
                    /*try {
                        Excel.read(file);
                        System.out.println(file.getName());
                    } catch (IOException ex) {
                        Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                    }*/
                } else {
                    JOptionPane.showMessageDialog(null, "Solo se aceptan archivos tipo .csv");
                }
            }

        });
    }

    private void btnManualEntryClick() {
        homeView.btnManualEntry.addActionListener(e -> {
            System.out.println("Fukc");
        });
    }

    private void btnAddContactClick() {
        homeView.btnAddContact.addActionListener(e -> {
            if (selectRowTable == -1) {
                //Nuevo contacto
                Contact contact = new Contact();

                contact.setIndicative(homeView.txtIndicative.getText());
                contact.setState("Activo");
                contact.setNumber(homeView.txtNumber.getText());
                contact.setMessage(homeView.txtMessage.getText());
                contact.setDefault_message(homeView.checkDefaultMessage.isSelected());

                homeView.txtNumber.setText("");
                homeView.txtMessage.setText("");
                homeView.txtIndicative.setText("");
                homeView.checkDefaultMessage.setSelected(false);

                contactDao.create(contact);

                homeView.addContactDialog.dispose();

                this.setDataTableContacts(null);
            } else {

                Long id = Long.parseLong(tableModelContact.getValueAt(selectRowTable, 0) + "");

                Contact contact = getContactByID(id);

                contact.setIndicative(homeView.txtIndicative.getText());
                contact.setNumber(homeView.txtNumber.getText());
                contact.setMessage(homeView.txtMessage.getText());
                contact.setDefault_message(homeView.checkDefaultMessage.isSelected());

                contactDao.update(contact);

                homeView.addContactDialog.dispose();

                this.setDataTableContacts(null);
            }
        });
    }

    private void btnDeleteContactClick() {
        homeView.btnDeleteContact.addActionListener(e -> {
            if (selectRowTable != -1) {

                int dialogResult = JOptionPane.showConfirmDialog(null, "¿Desea borrar este contacto?", "Confirmacion", 2);

                if (dialogResult == JOptionPane.YES_OPTION) {
                    Long id = Long.parseLong(tableModelContact.getValueAt(selectRowTable, 0) + "");
                    contactDao.delete(id);
                    tableModelContact.removeRow(selectRowTable);
                    selectRowTable = -1;
                }

            } else {
                JOptionPane.showMessageDialog(null, "Elija un contacto");
            }
        });
    }

    private void btnChangeStateContactClick() {
        homeView.btnChangeStateContact.addActionListener(e -> {
            if (selectRowTable != -1) {
                Long id = Long.parseLong(tableModelContact.getValueAt(selectRowTable, 0) + "");
                Contact contact = this.getContactByID(id);
                String new_state = "Activo".equals(contact.getState()) ? "Bloqueado" : "Activo";
                contact.setState(new_state);
                contactDao.update(contact);
                this.setDataTableContacts(null);
            } else {
                JOptionPane.showMessageDialog(null, "Elija un contacto");
            }
        });
    }

    private void btnEditContactClick() {
        homeView.btnEditContact.addActionListener(e -> {
            if (selectRowTable != -1) {

                Long id = Long.parseLong(tableModelContact.getValueAt(selectRowTable, 0) + "");

                Contact contact = this.getContactByID(id);

                homeView.txtNumber.setText(contact.getNumber());
                homeView.txtMessage.setText(contact.getMessage());
                homeView.txtIndicative.setText(contact.getIndicative());
                homeView.checkDefaultMessage.setSelected(contact.isDefault_message());

                homeView.addContactDialog.setLocationRelativeTo(null);
                homeView.addContactDialog.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Elija un contacto");
            }
        });
    }

    private void tableContactsClick() {
        homeView.tableContacts.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                selectRowTable = homeView.tableContacts.rowAtPoint(e.getPoint());
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
    }

    private void panelContentClick() {
        homeView.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                selectRowTable = -1;
                homeView.tableContacts.clearSelection();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
    }

    private Contact getContactByID(Long id) {
        //actualizar contacto
        return contacts.stream()
                .filter((c) -> Objects.equals(c.getId(), id))
                .findAny()
                .orElse(null);
    }

}
